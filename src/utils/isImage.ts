const isImage = (name: string): boolean => {
  const splitted = name.split(".");
  if (splitted[1] === "jpg") return true;
  else return false;
};

export default isImage;
