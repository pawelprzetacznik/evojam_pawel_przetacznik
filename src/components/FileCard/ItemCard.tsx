import React from "react";
import File from "../../models/File";
import "./itemCard.css";
import Directory from "../../models/Directory";
import IconDisplay from "../IconDisplay/IconDisplay";
import { useDispatch } from "react-redux";
import HistoryActionType from "../../store/actionTypes";

/**
 * React Components that displays information about files or directories
 */
const ItemCard = React.memo<{
  item: File | Directory;
}>(({ item }) => {
  const dispatch = useDispatch();
  const handleClick = () => {
    //fire only when item is of type Dictionary
    if ((item as Directory).id !== undefined) {
      dispatch({ type: HistoryActionType.ADD, payload: item as Directory });
    }
  };

  return (
    <div className="itemcard" onClick={handleClick}>
      <div className="itemcard-icon">
        <IconDisplay
          name={item.name}
          isDirectory={(item as Directory).id !== undefined}
        />
      </div>
      <div className="itemcard-name">
        <span>{item.name}</span>
      </div>
    </div>
  );
});

export default ItemCard;
