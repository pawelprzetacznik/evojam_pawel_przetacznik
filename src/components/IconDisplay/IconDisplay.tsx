import React from "react";
import {
  MdInsertDriveFile,
  MdOutlineImage,
  MdOutlineFolder,
} from "react-icons/md";
import isImage from "../../utils/isImage";

/**
 * React components that decides what kind of icon will be displayied in ItemCard
 */
const IconDisplay: React.FC<{ isDirectory: boolean; name: string }> = ({
  isDirectory,
  name,
}) => {
  if (isDirectory) return <MdOutlineFolder size={40} />;

  return isImage(name) ? (
    <MdOutlineImage size={40} />
  ) : (
    <MdInsertDriveFile size={40} />
  );
};

export default IconDisplay;
