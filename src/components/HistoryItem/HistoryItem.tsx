import React from "react";
import { useDispatch } from "react-redux";
import "./historyItem.css";
import HistoryActionType from "../../store/actionTypes";
/**
 * React Component that displays history item
 */
const HistoryItem: React.FC<{ name: string; index: number }> = ({
  name,
  index,
}) => {
  const dispatch = useDispatch();

  return (
    <div className="historyitem">
      <span
        onClick={() =>
          dispatch({ type: HistoryActionType.GO_TO, payload: index })
        }
      >
        {name}
      </span>{" "}
      /
    </div>
  );
};

export default HistoryItem;
