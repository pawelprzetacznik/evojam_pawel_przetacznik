import React, { useState } from "react";
import { useSelector } from "react-redux";
import { __API_LINK__ } from "../../exports";
import useFetch from "../../Hooks/useFetch";
import RootDirectory from "../../models/RootDirectory";
import { RootState } from "../../store/store";
import ItemCard from "../FileCard/ItemCard";
import HistoryItem from "../HistoryItem/HistoryItem";
import "./root.css";

const Root: React.FC = () => {
  const history = useSelector((state: RootState) => state.history);

  //data fetching
  const { data, loading, error } = useFetch<RootDirectory>(
    __API_LINK__ + "/directories/" + history[history.length - 1].id
  );

  if (loading) return <div className="root-message">Loading...</div>;
  if (error) return <div className="root-message">Error: {error}</div>;

  return data ? (
    <div className="root">
      <div className="root-history">
        {history.map((el, i) => (
          <HistoryItem index={i} name={el.name} key={i} />
        ))}
      </div>
      <div className="root-display">
        {/* I use index in map functions below just to avoid warining about keys, */}
        {/* because root folder contains files with the same names */}
        {data.directories.map((dir) => (
          <ItemCard item={dir} key={dir.name} />
        ))}
        {data.files.map((file, i) => (
          <ItemCard item={file} key={file.name + i} />
        ))}
      </div>
    </div>
  ) : (
    <></>
  );
};

export default Root;
