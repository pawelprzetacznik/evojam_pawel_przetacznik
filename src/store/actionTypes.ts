enum HistoryActionType {
  ADD = "history_add",
  GO_TO = "history_goto",
}

export default HistoryActionType;
