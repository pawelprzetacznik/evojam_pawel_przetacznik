import { createStore, combineReducers } from "redux";
import historyReducer from "./reducers/historyReducer";

const rootReducer = combineReducers({
  history: historyReducer,
});
export type RootState = ReturnType<typeof rootReducer>;
export default createStore(rootReducer);
