import { Reducer } from "redux";
import Directory from "../../models/Directory";
import HistoryAction from "../actions/HistoryActions";
import HistoryActionType from "../actionTypes";

const initState = [{ name: "root", id: "0" }];

const historyReducer: Reducer<Directory[], HistoryAction> = (
  state = initState,
  action
) => {
  switch (action.type) {
    case HistoryActionType.ADD:
      return [...state, action.payload];

    case HistoryActionType.GO_TO:
      return state.filter((el) => parseInt(el.id) <= action.payload);

    default:
      return state;
  }
};

export default historyReducer;
