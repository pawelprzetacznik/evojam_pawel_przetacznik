import Directory from "../../models/Directory";
import HistoryActionType from "../actionTypes";

interface Add {
  type: HistoryActionType.ADD;
  payload: Directory;
}

interface GoTo {
  type: HistoryActionType.GO_TO;
  payload: number;
}

type HistoryAction = Add | GoTo;

export default HistoryAction;
