import { useState, useEffect, useRef } from "react";
import ky, { HTTPError } from "ky";

/**
 * My custom React Hook that handles fetching data
 * @param url
 */
const useFetch = <T extends any>(url: string) => {
  const [data, setData] = useState<T>();
  const [loading, setLoading] = useState<boolean>();
  const [error, setError] = useState<string>();

  const mounted = useRef(true);
  useEffect(() => {
    return () => {
      mounted.current = false;
    };
  }, []);

  useEffect(() => {
    const fetch = async () => {
      try {
        setLoading(true);
        const res = (await ky.get(url).json()) as T;

        if (mounted.current) setData(res);
      } catch (err: any) {
        if (err instanceof HTTPError) {
          setError(err.message);
        }
      } finally {
        setLoading(false);
      }
    };
    fetch();
  }, [url]);

  return { data, loading, error };
};

export default useFetch;
