import Directory from "./Directory";
import File from "./File";

interface RootDirectory {
  name: string;
  id: string;
  files: File[];
  directories: Directory[];
}

export default RootDirectory;
