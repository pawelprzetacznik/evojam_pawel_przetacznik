interface Directory {
  id: string;
  name: string;
}

export default Directory;
