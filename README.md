Hello!
I used React and TypeScript to build this app so to run it two commands must be executed:

1. npm i or yarn install
2. npm start or yarn start

I also used Redux to handle path and to avoid prop drilling.

Have a nice day!
